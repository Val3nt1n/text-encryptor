package encryptApp.controller;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.crypto.*;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.awt.*;
import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ResourceBundle;

public class EncryptFilesController implements Initializable {

    @FXML
    private Hyperlink hyperLink;
    @FXML
    private CheckBox desBox;
    @FXML
    private CheckBox blowFishBox;
    @FXML
    private TextField secretKeyField;
    @FXML
    private JFXButton decryptBtn;
    @FXML
    private JFXButton encryptBtn;
    @FXML
    private JFXButton backBtn;
    @FXML
    private TextField textFieldFile;
    @FXML
    private JFXButton loadFileBtn;
    @FXML
    private JFXButton closeBtn;
    public AnchorPane anchorPane;

    File encrypted = new File(System.getProperty("user.home") + System.getProperty("file.separator") +
            "Desktop" + System.getProperty("file.separator") + "encryptedFile.txt");

    File decrypted = new File(System.getProperty("user.home") + System.getProperty("file.separator") +
            "Desktop" + System.getProperty("file.separator") + "DecryptedFile.txt");

    private static SecretKey mySecretDesKey;
    private static SecretKey mySecretBlowFishKey;

    public void openMyLink(ActionEvent mouseEvent) {
        openWebpage("https://gitlab.com/Val3nt1n/");
    }

    public static void openWebpage(String url) {
        try {
            Desktop.getDesktop().browse(new URL(url).toURI());
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public void backActionButton(ActionEvent actionEvent) {
        backBtn.getScene().getWindow().hide();
    }

    public void checkBox(ActionEvent actionEvent) {
        if (actionEvent.getSource() == desBox) {
            blowFishBox.setDisable(true);
            encryptBtn.setDisable(false);
            decryptBtn.setDisable(false);
            secretKeyField.setDisable(false);
            if (!desBox.isSelected()) {
                blowFishBox.setDisable(false);
                encryptBtn.setDisable(true);
                decryptBtn.setDisable(true);
                secretKeyField.setDisable(true);
            }
        } else if (actionEvent.getSource() == blowFishBox) {
            desBox.setDisable(true);
            encryptBtn.setDisable(false);
            decryptBtn.setDisable(false);
            secretKeyField.setDisable(false);
            if (!blowFishBox.isSelected()) {
                desBox.setDisable(false);
                encryptBtn.setDisable(true);
                decryptBtn.setDisable(true);
                secretKeyField.setDisable(true);
            }
        }
    }

    // Generate Des SecretKey
    public static void secretDesKey(String myDesKey) {
        try {
            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("DES");
            byte[] dkeyBytes = myDesKey.getBytes();
            DESKeySpec desKeySpec = new DESKeySpec(dkeyBytes);
            mySecretDesKey = secretKeyFactory.generateSecret(desKeySpec);

        } catch (NoSuchAlgorithmException | InvalidKeyException | InvalidKeySpecException e) {
            e.printStackTrace();
        }

    }

    // Generate BlowFish SecretKey
    public static void secretBlowFishKey(String myBLKey) {
        try {
            SecretKey secretKey = new SecretKeySpec(myBLKey.getBytes(), "Blowfish");
            mySecretBlowFishKey = secretKey;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Close button
    public void close(ActionEvent actionEvent) {
        System.exit(0);
    }

    // Load file method
    public void getFileToEncryptOrDecrypt(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        loadFileBtn.setOnAction(e -> {
            fileChooser.setTitle("Load file to encrypt!");
            Stage stage = (Stage) anchorPane.getScene().getWindow();
            File selectedFile = fileChooser.showOpenDialog(stage);
            if (selectedFile == null) {
                Alert alert = new Alert(Alert.AlertType.WARNING, "You need to load a file to encrypt his content!");
                alert.showAndWait();
            } else {
                textFieldFile.setText(selectedFile.getAbsolutePath());
            }
        });
    }


    private static void write(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[64];
        int numOfBytesRead;
        while ((numOfBytesRead = in.read(buffer)) != -1) {
            out.write(buffer, 0, numOfBytesRead);
        }
        out.close();
        in.close();
    }

    public void encryptFile(ActionEvent actionEvent) {
        File toEncrypt = new File(textFieldFile.getText());
        if (desBox.isSelected()) {
            encryptDes(toEncrypt, encrypted);
        } else if (blowFishBox.isSelected()) {
            encryptBlowFish(toEncrypt, encrypted);
        }
    }

    public void decryptFile(ActionEvent actionEvent) {
        File toDecrypt = new File(textFieldFile.getText());
        if (desBox.isSelected()) {
            decryptDes(toDecrypt, decrypted);
        } else if (blowFishBox.isSelected()) {
            decryptBlowFish(toDecrypt, decrypted);
        }
    }

    // Encrypt with Des
    public void encryptDes(File in, File out) {
        if (secretKeyField.getText().isEmpty() && secretKeyField.getText().length() < 8) {
            Alert alert = new Alert(Alert.AlertType.WARNING, "You need a secret key more then 8 char!");
            alert.showAndWait();
        }
        try {
            FileInputStream fis = new FileInputStream(in);
            FileOutputStream fos = new FileOutputStream(out);
            secretDesKey(secretKeyField.getText());
            Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, mySecretDesKey);
            CipherInputStream cis = new CipherInputStream(fis, cipher);
            write(cis, fos);
        } catch (NoSuchAlgorithmException | InvalidKeyException | NoSuchPaddingException | IOException e) {
            e.printStackTrace();
        }
    }

    // Decrypt with Des
    public void decryptDes(File in, File out) {
        if (secretKeyField.getText().isEmpty() && secretKeyField.getText().length() < 8) {
            Alert alert = new Alert(Alert.AlertType.WARNING, "You need a secret key more then 8 char!");
            alert.showAndWait();
        }
        try {
            FileInputStream fis = new FileInputStream(in);
            FileOutputStream fos = new FileOutputStream(out);
            secretDesKey(secretKeyField.getText());
            Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, mySecretDesKey);
            CipherInputStream cis = new CipherInputStream(fis, cipher);
            write(cis, fos);
        } catch (NoSuchAlgorithmException | InvalidKeyException | NoSuchPaddingException | IOException e) {
            e.printStackTrace();
        }
    }

    // Encrypt with BlowFish
    public void encryptBlowFish(File in, File out) {
        if (secretKeyField.getText().isEmpty() && secretKeyField.getText().length() < 10) {
            Alert alert = new Alert(Alert.AlertType.WARNING, "You need a secret key more then 10 char!");
            alert.showAndWait();
        }
        try {
            FileInputStream fis = new FileInputStream(in);
            FileOutputStream fos = new FileOutputStream(out);
            secretBlowFishKey(secretKeyField.getText());
            Cipher cipher = Cipher.getInstance("BlowFish/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, mySecretBlowFishKey);
            CipherInputStream cis = new CipherInputStream(fis, cipher);
            write(cis, fos);
        } catch (NoSuchAlgorithmException | InvalidKeyException | NoSuchPaddingException | IOException e) {
            e.printStackTrace();
        }
    }

    // Decrypt with BlowFish
    public void decryptBlowFish(File in, File out) {
        if (secretKeyField.getText().isEmpty() && secretKeyField.getText().length() < 10) {
            Alert alert = new Alert(Alert.AlertType.WARNING, "You need a secret key more then 10 char!");
            alert.showAndWait();
        }
        try {
            FileInputStream fis = new FileInputStream(in);
            FileOutputStream fos = new FileOutputStream(out);
            secretBlowFishKey(secretKeyField.getText());
            Cipher cipher = Cipher.getInstance("BlowFish/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, mySecretBlowFishKey);
            CipherInputStream cis = new CipherInputStream(fis, cipher);
            write(cis, fos);
        } catch (NoSuchAlgorithmException | InvalidKeyException | NoSuchPaddingException | IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        encryptBtn.setDisable(true);
        decryptBtn.setDisable(true);
        secretKeyField.setDisable(true);
    }
}
