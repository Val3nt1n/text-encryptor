package encryptApp.controller;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import javax.crypto.*;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.awt.*;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Base64;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Controller implements Initializable {
    @FXML
    private Hyperlink hyperLink;
    @FXML
    private JFXButton encryptFiles;
    @FXML
    private TextArea encryptedTextArea;
    @FXML
    private JFXButton close;
    @FXML
    private TextField textField;
    @FXML
    private TextArea textArea;
    @FXML
    private CheckBox desBox;
    @FXML
    private CheckBox blowFishBox;
    @FXML
    private CheckBox aesBox;
    @FXML
    private JFXButton encryptBtn;
    @FXML
    private JFXButton decryptBtn;

    final static Charset CHARSET_ISO_8859_1 = Charset.forName("ISO-8859-1");


    private static SecretKeySpec secretKey;
    private static byte[] key;
    private static SecretKey mySecretDesKey;
    private static SecretKey mySecretBlowFishKey;

    //Generate AES SecretKey
    public static void setAesKey(String myKey) {
        MessageDigest sha = null;
        try {
            key = myKey.getBytes(CHARSET_ISO_8859_1);
            sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);
            secretKey = new SecretKeySpec(key, "AES");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    // Generate Des SecretKey
    public static void secretDesKey(String myDesKey) {
        try {
            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("DES");
            byte[] dkeyBytes = myDesKey.getBytes();
            DESKeySpec desKeySpec = new DESKeySpec(dkeyBytes);
            mySecretDesKey = secretKeyFactory.generateSecret(desKeySpec);

        } catch (NoSuchAlgorithmException | InvalidKeyException | InvalidKeySpecException e) {
            e.printStackTrace();
        }

    }

    // Generate BlowFish SecretKey
    public static void secretBlowFishKey(String myBLKey) {
        try {
            Key secretKey = new SecretKeySpec(myBLKey.getBytes(), "Blowfish");
            mySecretBlowFishKey = (SecretKey) secretKey;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // CheckBox Validation
    public void checkBox(ActionEvent actionEvent) {
        if (actionEvent.getSource() == desBox) {
            blowFishBox.setDisable(true);
            aesBox.setDisable(true);
            encryptBtn.setDisable(false);
            decryptBtn.setDisable(false);
            textField.setDisable(false);
            if (!desBox.isSelected()) {
                blowFishBox.setDisable(false);
                aesBox.setDisable(false);
                encryptBtn.setDisable(true);
                decryptBtn.setDisable(true);
                textField.setDisable(true);
            }
        } else if (actionEvent.getSource() == blowFishBox) {
            desBox.setDisable(true);
            aesBox.setDisable(true);
            encryptBtn.setDisable(false);
            decryptBtn.setDisable(false);
            textField.setDisable(false);
            if (!blowFishBox.isSelected()) {
                desBox.setDisable(false);
                aesBox.setDisable(false);
                encryptBtn.setDisable(true);
                decryptBtn.setDisable(true);
                textField.setDisable(true);
            }
        } else if (actionEvent.getSource() == aesBox) {
            desBox.setDisable(true);
            blowFishBox.setDisable(true);
            textField.setDisable(true);
            encryptBtn.setDisable(false);
            decryptBtn.setDisable(false);
            if (!aesBox.isSelected()) {
                desBox.setDisable(false);
                blowFishBox.setDisable(false);
                textField.setDisable(true);
                encryptBtn.setDisable(true);
                decryptBtn.setDisable(true);
            }
        }
    }

    // Close button
    public void close(ActionEvent actionEvent) {
        System.exit(0);
    }


    // Encrypt function
    public String encrypt(ActionEvent actionEvent) {
        if (textArea.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING, "Text Area it's empty!");
            alert.showAndWait();
        } else if (aesBox.isSelected()) {
            try {
                setAesKey(textField.getText());
                Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
                cipher.init(Cipher.ENCRYPT_MODE, secretKey);
                encryptedTextArea.setText(Base64.getEncoder().encodeToString(cipher.doFinal(textArea.getText().getBytes(CHARSET_ISO_8859_1))));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (desBox.isSelected()) {
            if (textField.getText().length() < 8) {
                Alert alert = new Alert(Alert.AlertType.WARNING, "Secret key need to be more then 10 chars");
                alert.showAndWait();
            } else {
                try {
                    secretDesKey(textField.getText());
                    Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
                    cipher.init(Cipher.ENCRYPT_MODE, mySecretDesKey);
                    encryptedTextArea.setText(Base64.getEncoder().encodeToString(cipher.doFinal(textArea.getText().getBytes(CHARSET_ISO_8859_1))));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (blowFishBox.isSelected()) {
            if (textField.getText().length() < 10) {
                Alert alert = new Alert(Alert.AlertType.WARNING, "Secret key need to be more then 10 chars");
                alert.showAndWait();
            } else {
                try {
                    secretBlowFishKey(textField.getText());
                    Cipher cipher = Cipher.getInstance("BlowFish/ECB/PKCS5Padding");
                    cipher.init(Cipher.ENCRYPT_MODE, mySecretBlowFishKey);
                    encryptedTextArea.setText(Base64.getEncoder().encodeToString(cipher.doFinal(textArea.getText().getBytes(CHARSET_ISO_8859_1))));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
        return null;
    }

    // Decrypt function
    public String decrypt(ActionEvent actionEvent) {
        if (encryptedTextArea.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING, "Text Area it's empty!");
            alert.showAndWait();
        }
        if (aesBox.isSelected()) {
            try {
                setAesKey(textField.getText());
                Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
                cipher.init(Cipher.DECRYPT_MODE, secretKey);
                textArea.setText(new String(cipher.doFinal(Base64.getDecoder().decode(encryptedTextArea.getText()))));
            } catch (Exception e) {
                System.out.println("Error while decrypting: " + e.toString());
            }
        } else if (desBox.isSelected()) {
            if (textField.getText().length() < 8) {
                Alert alert = new Alert(Alert.AlertType.WARNING, "Secret key need to be more then 10 chars");
                alert.showAndWait();
            } else {
                try {
                    secretDesKey(textField.getText());
                    Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
                    cipher.init(Cipher.DECRYPT_MODE, mySecretDesKey);
                    byte[] dec = Base64.getDecoder().decode(encryptedTextArea.getText().getBytes(CHARSET_ISO_8859_1));
                    //Decrypt
                    byte[] utf8 = cipher.doFinal(dec);
                    //Decode using utf-8
                    textArea.setText(new String(utf8, StandardCharsets.UTF_8));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } else if (blowFishBox.isSelected()) {
            if (textField.getText().length() < 10) {
                Alert alert = new Alert(Alert.AlertType.WARNING, "Secret key need to be more then 10 chars");
                alert.showAndWait();
            } else {
                try {
                    secretBlowFishKey(textField.getText());
                    Cipher cipher = Cipher.getInstance("Blowfish/ECB/PKCS5Padding");
                    cipher.init(Cipher.DECRYPT_MODE, mySecretBlowFishKey);
                    byte[] utf8 = cipher.doFinal(Base64.getDecoder().decode(encryptedTextArea.getText().getBytes()));
                    textArea.setText(new String(utf8, CHARSET_ISO_8859_1));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public void openMyLink(ActionEvent mouseEvent) {
        openWebpage("https://gitlab.com/Val3nt1n/");
    }

    public static void openWebpage(String url) {
        try {
            Desktop.getDesktop().browse(new URL(url).toURI());
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        encryptBtn.setDisable(true);
        decryptBtn.setDisable(true);
        textField.setDisable(true);
        encryptFiles.setOnMouseClicked((event) -> {
            try {
                Parent root = FXMLLoader.load(getClass().getResource("/fxml/files.fxml"));
                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.initStyle(StageStyle.TRANSPARENT);
                Image icon = new Image("icon.png");
                stage.getIcons().add(icon);
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.setScene(scene);
                stage.show();
            } catch (IOException e) {
                Logger logger = Logger.getLogger(getClass().getName());
                logger.log(Level.SEVERE, "Failed to create new Window.", e);
            }
        });
    }
}



